run:
	npm run build -- --watch

clean:
	rm -r node_modules

init: clean
	git pull
	npm install
