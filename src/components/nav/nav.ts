import Vue from 'vue'
import Component from 'vue-class-component'
import { Prop, Model } from 'vue-property-decorator'
require('./nav.scss');


class NavItem {
    constructor(text: string, href: string) {
        this._text = text;
        this._href = href;
    }

    private _text: string;
    private _href: string;

    get text(): string {
        return this._text;
    }
    get href(): string {
        return this._href;
    }
}


@Component({
    template: require('./nav.htm')
  })
class NavComponent extends Vue{
    constructor() {
        super();
        let aboutme = new NavItem('About Me', '#aboutme');
        this._navpoints = [];
        this._navpoints.push(aboutme);
    }

    private _navpoints: NavItem[];

    get navpoints(): NavItem[] { 
        return this._navpoints;
    }

}

export default NavComponent