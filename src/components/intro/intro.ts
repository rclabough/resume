import Vue from 'vue'
import Component from 'vue-class-component'
import { Prop, Model } from 'vue-property-decorator'
require('./intro.scss');

@Component({
    template: require('./intro.htm')
  })
class IntroComponent extends Vue{

    get name(): string { 
        return 'Robert Clabough';
    }

}

export default IntroComponent