import Vue from 'vue'
import Component from 'vue-class-component'
import { Prop, Model, Watch } from 'vue-property-decorator'
import Promise from 'Promise'
import NavComponent from '../nav/nav'
import Project from '../../data/types/project';
import Skill from '../../data/types/skill'
import DataService from '../../data/data-service';
require('./projects.scss');

@Component({
    template: require('./projects.htm'),
    components: { NavComponent }
  })
class ProjectsComponent extends Vue{
    
    skills: Skill[] = DataService.instance().skills;
    
    filteredSkills: string[] = this.initFilteredSkills();
    projects: Project[] = this.updateProjects();

    private containsAny(source: string[], str: string): boolean {
        return source.some(k => this.equalsIgnoreCase(k, str));
    }
    private containsCross(source: string[], matchAny: string[]) {
        return source.filter
    }
    private equalsIgnoreCase(left: string, right: string): boolean {
        if (!left || !right) {
            return false;
        }
        return left.toUpperCase() === right.toUpperCase();
    }
    private containsIgnoreCase(left: string, right: string): boolean {
        if (!left || !right) {
            return false;
        }
        var found: number = right.toUpperCase().indexOf(left.toUpperCase());
        var isFound: boolean = (found !== -1);
        return isFound;
    }

    private initFilteredSkills(): string[] {
        var arr: string[] = [];
        var queryParam = this.$router.currentRoute.query.skill;
        if (queryParam) {
            arr.push(queryParam);
        }
        this.filteredSkills = arr;
        this.updateProjects();
        return arr;
    }
    addSkill(skill: string) {
        if (!this.filteredSkills) {
            this.filteredSkills = [];
        }
        Vue.set(this.filteredSkills, this.filteredSkills.length, skill);
        this.filteredSkills = this.filteredSkills.sort();
        this.updateProjects();
        //this._filteredSkills.push(skill);
        console.log(`Skills with ${skill} are now: ${this.filteredSkills.join(',')}`);
    }
    clearFilters() {
        this.filteredSkills = [];
        this.updateProjects();
    }

    private updateProjects(): Project[] {
        var me = this;
        var filters = me.filteredSkills.filter(k => k).map(k => k.trim()).slice(0) || [];
        var arr: Project[] = DataService.instance().projects
            .filter(k => filters.length === 0 || filters.every(j => me.containsAny(k.skills, j)))
            .sort((left, right) => left.name.localeCompare(right.name));
        me.projects = arr;
        return arr;
    }
    private found(keyword: string): boolean {
        var skillNames: string[] = this.skills.map(k => k.name);
        return this.containsAny(skillNames, keyword);
    }

    removeFilter(skill: string): void {
        this.filteredSkills = this.filteredSkills.filter(k => !this.equalsIgnoreCase(k, skill));
        this.updateProjects();
    }
    fetch(keyword: string): any {
        return new Promise((resolve: any, reject: any) => resolve(keyword));
    }
    getResponse(keyword: any): any {
        return this.skills
            .map(k => k.name)
            .filter(k => this.containsIgnoreCase(keyword, k))
            .filter(k => !this.containsAny(this.filteredSkills, k));
    }
    hit(keyword: string, unknown: any, e: string) {
        //Update the filters!
        debugger;
        if (typeof e === 'undefined' && (!this.model || !this.found(this.model))) {
            unknown.query = keyword;
        } else {
            this.addSkill(keyword);
            this.model = undefined;
            unknown.query = '';
        }
    }
    prettyRepository(repoType: string): string {
        if (repoType === 'bitbucket') {
            return 'BitBucket';
        }
        if (repoType === 'github') {
            return 'GitHub';
        }
        return '';
    }


    private _model: string | undefined;

    get model(): string | undefined {
        return this._model || '';
    }
    set model(value: string | undefined) {
        this._model = value;
    }
}

export default ProjectsComponent