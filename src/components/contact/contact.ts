import Vue from 'vue'
import Component from 'vue-class-component'
import { Prop, Model } from 'vue-property-decorator'
import ContactService from '../../services/contact-service'
import Contact from '../../data/types/contact'
import Address from '../../data/types/address'
require('./contact.scss');

@Component({
    template: require('./contact.htm')
  })
class ContactComponent extends Vue{
    private _contact: Contact;

    get contact(): Contact {
        if (!this._contact) {
            this._contact = new ContactService().contact;
        }
        return this._contact;
    }
    get address(): Address {
        return this._contact.address;
    }
}

export default ContactComponent