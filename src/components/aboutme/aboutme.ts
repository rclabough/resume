import Vue from 'vue'
import Component from 'vue-class-component'
import { Prop, Model } from 'vue-property-decorator'
require('./aboutme.scss');

@Component({
    template: require('./aboutme.htm')
  })
class AboutMeComponent extends Vue{
}

export default AboutMeComponent