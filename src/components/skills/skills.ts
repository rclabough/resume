import Vue from 'vue'
import Component from 'vue-class-component'
import { Prop, Model } from 'vue-property-decorator'
import Skill from '../../data/types/skill'
import SkillService from '../../services/skills-service'
import SkillModel from '../../models/skillmodel'
require('./skills.scss');

@Component({
    template: require('./skills.htm')
  })
class SkillsComponent extends Vue{

    private _skillService: SkillService;
    private _showAll: boolean = false;

    private init(): SkillModel[] {
        this._showAll = false;
        if (!this._skillService) {
            this._skillService = new SkillService();
        }
        this.aToZ();
        return this.skills;
    }

    private initCategories(): string[] {
        if (!this._skillService) {
            this._skillService = new SkillService();
        }
        this.categories = this._skillService.categories.sort((left, right) => left.localeCompare(right));
        return this.categories;
    }

    aToZ(): void {
        this.skills = this._skillService.skills
            .sort((left, right) => left.skill.name.localeCompare(right.skill.name));
    }

    zToA(): void {
        this.skills = this._skillService.skills
            .sort((left, right) => right.skill.name.localeCompare(left.skill.name));
    }

    showAll(): void {
        for(var index in this.checks) {
            this.checks[index] = false;
        }
        this._showAll = true;
    }
    showFilters(): void {
        this._showAll = false;
    }

    filteredSkills(): SkillModel[] {
        debugger;
        if (this._showAll) {
            return this.skills;
        }

        var none: boolean = false;
        for(var index in this.checks) {
            none = none || this.checks[index];
        }
        var arr: SkillModel[] = this.init();
        if (!none) {
            return arr;
        }
        return this.skills.filter(skill => skill.skill.categories.some(cat => this.checks[cat]));
      }

    skills: SkillModel[] = this.init();
    categories: string[] = this.initCategories();
    checks: { [key:string]:boolean; } = {};

    options: any = {
        dropzoneSelector: 'ul',
        draggableSelector: 'li',
        excludeOlderBrowsers: true,
        multipleDropzonesItemsDraggingEnabled: true
    }
}

export default SkillsComponent