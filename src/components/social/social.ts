import Vue from 'vue'
import Component from 'vue-class-component'
import { Prop, Model } from 'vue-property-decorator'
require('./social.scss');

@Component({
    template: require('./social.htm')
  })
class SocialComponent extends Vue {
}

export default SocialComponent