import Vue from 'vue'
import Component from 'vue-class-component'
import { Prop } from 'vue-property-decorator'
import NavComponent from '../nav/nav'
import IntroComponent from '../intro/intro'
import AboutMeComponent from '../aboutme/aboutme'
import SkillsComponent from '../skills/skills'
import SocialComponent from '../social/social'
import ContactComponent from '../contact/contact'
require('./main.scss');

@Component({
    template: require('./main.htm'),
    components: { NavComponent, IntroComponent, AboutMeComponent, SkillsComponent, SocialComponent, ContactComponent }
  })
class MainComponent extends Vue{
}

export default MainComponent