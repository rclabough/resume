
import Component from 'vue-class-component'
import Vue from 'vue'
import VueRouter from 'vue-router'
import ParallaxJs from 'vue-parallax-js'
import BootstrapVue from 'bootstrap-vue'
import TypeAhead from 'vue2-typeahead'
import VueSmoothScroll from 'vue-smooth-scroll'
import VueDraggable from 'vue-draggable'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'vue-awesome/icons'
import Icon from 'vue-awesome/components/Icon'
require('./style.scss');
import MainComponent from './components/main/main'
import ProjectsComponent from './components/projects/projects'



// add vue-parallax-js to vue
Vue.use(ParallaxJs);
Vue.use(VueDraggable);
Vue.use(VueRouter);
Vue.use(BootstrapVue);
Vue.use(VueSmoothScroll);
Vue.component('TypeAhead', TypeAhead);
Vue.component('icon', Icon)

const router = new VueRouter({
  routes: [
    { path: '/', component: MainComponent },
    { path: '/main', component: MainComponent },
    { path: '/projects', component: ProjectsComponent, props: true }
  ]
})

new Vue({
    router
  }).$mount('#app')