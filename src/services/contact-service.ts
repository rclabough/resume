﻿import Contact from '../data/types/contact'
import DataService from '../data/data-service'

export default class ContactService {
    constructor() {
        this._dataService = DataService.instance();
    }
    private _dataService: DataService;

    get contact(): Contact {
        return this._dataService.contact;
    }
}