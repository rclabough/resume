import Skill from '../data/types/skill'
import Project from '../data/types/project'
import SkillModel from '../models/skillmodel'
import DataService from '../data/data-service'

export default class SkillService {
    constructor() {
        this.init();
    }

    private _skills: SkillModel[];

    private init() {
        var projects = DataService.instance().projects;
        this._skills = DataService.instance().skills.map(skill => {
            var knownProjects: Project[] = projects
                .filter(k => k.skills.some(j => j.toUpperCase() === skill.name.toUpperCase()));
            return new SkillModel(skill, knownProjects);
        });
    }

    get skills(): SkillModel[] {
        return this._skills;
    }

    get categories(): string[] {
        var arr = this._skills.map(k => k.skill)
            .filter(k => k.categories)
            .filter(k => k.categories.length > 0)
            .map(k => k.categories)
            .reduce((left, right) => left.concat(right));
        var seen: string[] = [];
        arr.forEach(value => {
            if (seen.indexOf(value) === -1) {
                seen.push(value);
            }
        });
        return seen;
    }

}
