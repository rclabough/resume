declare module 'bootstrap-vue'{
    const value: any;
    export default value;
}

declare module 'vue-awesome/components/Icon' {
    const value: any;
    export default value;
}

declare module 'vue-parallax-js' {
    const value: any;
    export default value;
}

declare module 'vue2-typeahead' {
    const value: any;
    export default value;
}

declare module 'Promise' {
    const value: any;
    export default value;
}

declare module 'vue-smooth-scroll' {
    const value: any;
    export default value;
}

declare module 'vue-draggable' {
    const value: any;
    export default value;
}
