﻿import Address from './address'

export default class Contact {
    private _address: Address;
    private _phone: string;
    private _email: string;

    set address(value: Address) {
        this._address = value;
    }
    set phone(value: string) {
        this._phone = value;
    }
    set email(value: string) {
        this._email = value;
    }
    get address(): Address {
        return this._address;
    }
    get phone(): string {
        return this._phone;
    }
    get email(): string {
        return this._email;
    }
}