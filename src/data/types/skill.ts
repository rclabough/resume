export default class Skill {
    private _name: string;
    private _link: string | undefined;
    private _categories: string[];

    set name(value: string) {
        this._name = value;
    }

    set link(value: string) {
        this._link = value;
    }

    set categories(value: string[]) {
        this._categories = value;
    }

    get name(): string {
        return this._name;
    }

    get href(): string | undefined {
        return this._link;
    }

    get categories(): string[] {
        return this._categories;
    }
}