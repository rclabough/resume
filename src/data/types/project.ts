export default class Project {

    private _name: string;
    private _description: string;
    private _skills: string[];
    private _repository: string;
    private _images: string[];

    set name(value: string) {
        this._name = value;
    }

    set skills(value: string[]) {
        this._skills = value;
    }

    set description(value: string) {
        this._description = value;
    }

    set repository(value: string) {
        this._repository = value;
    }

    set images(value: string[]) {
        this._images = value;
    }

    get name(): string {
        return this._name;
    }

    get skills(): string[] {
        return this._skills;
    }

    get description(): string {
        return this._description;
    }

    get repository(): string {
        return this._repository;
    }

    get images(): string[] {
        return this._images;
    }
}