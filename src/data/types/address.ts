﻿export default class Address {
    private _street1: string;
    private _street2: string;
    private _state: string;
    private _city: string;
    private _zip: string;

    set street1(value: string) {
        this._street1 = value;
    }
    set street2(value: string) {
        this._street2 = value;
    }
    set city(value: string) {
        this._city = value;
    }
    set state(value: string) {
        this._state = value;
    }
    set zip(value: string) {
        this._zip = value;
    }
    get street1(): string {
        return this._street1;
    }
    get street2(): string {
        return this._street2;
    }
    get city(): string {
        return this._city;
    }
    get state(): string {
        return this._state;
    }
    get zip(): string {
        return this._zip;
    }
}