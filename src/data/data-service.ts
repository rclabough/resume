import Skill from './types/skill'
import Project from './types/project'
import Contact from './types/contact'

class DataSchema {
    private _projects: Project[];
    private _skills: Skill[];
    private _contact: Contact;

    set skills(value: Skill[]) {
        this._skills = value;
    }

    set projects(value: Project[]) {
        this._projects = value;
    }

    set contact(value: Contact) {
        this._contact = value;
    }

    get skills(): Skill[] {
        return this._skills;
    }

    get projects(): Project[] {
        return this._projects;
    }

    get contact(): Contact {
        return this._contact;
    }
}


export default class DataService {
    private static _instance: DataService;

    static instance(): DataService {
        if (!DataService._instance) {
            DataService._instance = new DataService();
        }
        return DataService._instance;
    }

    private constructor() {
        this._data = require("../data/datastore.json");
    }

    private _data: DataSchema;

    get skills(): Skill[] {
        return this._data.skills;
    }

    get projects(): Project[] {
        return this._data.projects;
    }

    get contact(): Contact {
        return this._data.contact;
    }
}