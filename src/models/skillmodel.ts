import Skill from '../data/types/skill'
import Project from '../data/types/project'

export default class SkillModel {

    constructor(skill: Skill, projects: Project[]) {
        this._skill = skill;
        this._projects = projects;
    }

    private _skill: Skill;
    private _projects: Project[];

    set skill(value: Skill) {
        this._skill = value;
    }

    set projects(value: Project[]) {
        this._projects = value;
    }

    get skill(): Skill {
        return this._skill;
    }

    get projects() : Project[] {
        return this._projects;
    }
}